<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return Inertia::render('Users', [
            'users' => $users->map(function ($user) {
                return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'edit_url' => $user->can('edit_delete', $user) ? URL::route('users.edit', $user->id) : false,
                'delete_url' => $user->can('edit_delete', $user) ? URL::route('users.destroy', $user->id) : false,
                ];
            }),
            'create_url' => URL::route('users.index'),
        ])->withViewData(['title' => 'Laravel Inertia']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('thumbnail')->getClientOriginalName()); 
        $this->validate($request, [
            'name' => ['required'],
            'email'=> ['required','email','unique:users'],
            'thumbnail' => ['image', 'mimes:png,jpg,jpeg'],
            'password' => ['required','confirmed'],
            'password_confirmation' => ['required']
        ]);
        
        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->thumbnail->store('images', 'public');
        }
        User::create([
            'name' => $request->name,
            'email'=> $request->email,
            'thumbnail' => $thumbnail,
            'password' => bcrypt($request->password),
        ]);
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if($user->can('edit_delete',$user)){
            return Inertia::render('Edit', [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email
            ]);
        }
        return redirect()->route('users.index');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required'],
            'email'=> ['required','email','unique:users,email,'.$id.',id'],
            
        ]);
        User::find($id)->update($request->all());
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->can('edit_delete')) {
            $user->delete();
        }
        return redirect()->route('users.index');
    }
}
