import { InertiaLink } from "@inertiajs/inertia-react";
import React from "react";
import Front from "../Layouts/Front";
const Home = () => {
    
    return (
        <Front title="Home Page">
            <h2>This is inertia app</h2>
            
            <InertiaLink href="/welcome">Welcome</InertiaLink>
        </Front>
    );
};

export default Home;
