import React from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import Front from "../Layouts/Front";
const Users = ({ users, create_url }) => {
    const { isLoggedIn } = usePage().props.user
    return (
        <Front title="User Page">
            {users.map(user =>{
                return (<div key={user.id}>
                
                    <p>
                        {user.id} - {user.name} -{" "}</p>
                    {(isLoggedIn && user.edit_url && user.delete_url) && <p>
                    <InertiaLink href={user.edit_url}>Edit</InertiaLink> || 
                        <InertiaLink replace method="POST" data={{ _method:'delete' }} href={user.delete_url}>Delete</InertiaLink>
                    </p>
                    }
                </div> 
                )
            }
                )}
        </Front>
    );
};

export default Users;
